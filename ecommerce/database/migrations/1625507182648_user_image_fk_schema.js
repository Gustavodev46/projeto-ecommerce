'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserImageFkSchema extends Schema {
    up() {
        this.table('users', (table) => {
            // alter table
            table
                .foreign('image_id') //chave estrangeira vai ser "image_id"
                .references('id') // referenciando o "id"
                .inTable('images') // da tabela "images"
                .onDelete('cascade') // quando deletar deleta todos dados do relacionamento

        })
    }

    down() {
        this.table('user', (table) => {
            // reverse alternations
            table.dropForeign('image_id')
        })
    }
}

module.exports = UserImageFkSchema