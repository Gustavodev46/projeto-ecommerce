'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PasswordReset extends Model {
    static boot() {
        super.boot()


        this.addHook('beforeCreate', async model => {
            model.token = await str_random(25)
            const expired_at = new Date()
            expired_at.setMinutes(expired_at.getMinutes() + 30)
            model.expired_at = expired_at
        })
    }

    // formata os valores para o formato de data que os bancos aceitam
    static get dates() {
        return ['created_at', 'updated_at', 'expired_at']
    }

}

module.exports = PasswordReset